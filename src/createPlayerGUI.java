import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class createPlayerGUI implements Runnable{

    JTextArea ta;
    JTextArea friendName;
    JLabel tResultOfAddingFriend;
    Player p = new Player("AaronC");

    public void run()
    {
        JFrame frame = new JFrame("Welcome!");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(600, 600));
        frame.setLocation(300, 300);
        frame.pack();
        frame.setVisible(true);

        JLabel tlbl = new JLabel();
        tlbl.setBounds(50, 100, 200, 30);
        tlbl.setText("Enter a name to search for or leave the textbox empty to test Threads");

        JLabel tResult = new JLabel();
        tResult.setBounds(50, 350, 250, 30);

        ta = new JTextArea();
        ta.setBounds(50, 300, 200, 30);

        JPanel panel = new JPanel();
        panel.setSize(450, 450);
        panel.setLocation(70, 200);

        JButton button = new JButton("Run");

        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String taVal = ta.getText();
                if(taVal.equals("") || taVal.equals(null)){
                    tResult.setText(AddAndRemoveUsers());
                }else {
                    ta.setText("");
                    Thread traditionalThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            tResult.setText(p.DoesFriendExist(taVal));
                        }
                    });
                    traditionalThread.start();
                }
            }
        });



        JLabel friendToAdd = new JLabel();
        friendToAdd.setBounds(50, 75, 200, 30);
        friendToAdd.setText("Enter a name add a friend(for multiple friends, add commas between them)");

        tResultOfAddingFriend = new JLabel();
        tResultOfAddingFriend.setBounds(50, 100, 250, 30);

        friendName = new JTextArea();
        friendName.setBounds(50, 50, 200, 30);

        JPanel panelFriendAdd = new JPanel();
        panelFriendAdd.setSize(200, 200);
        panelFriendAdd.setLocation(70, 0);

        JButton buttonAddF = new JButton("Add Friend");

        buttonAddF.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String fName = friendName.getText();
                if(fName.equals("") || fName.equals(null)){
                    tResultOfAddingFriend.setText("The name Field Cannot be null or empty!");
                }else {
                    friendName.setText("");
                    String[] parsedFNs = fName.split(",");
                    if(parsedFNs.length > 1){
                        addMultipleFriends(parsedFNs);
                    }else {
                        Thread traditionalThread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                if (p.addToFriendsList(fName)) {
                                    tResultOfAddingFriend.setText("Added: " + fName + " to your friends list!");
                                }
                            }
                        });
                        traditionalThread.start();
                    }
                }
            }
        });

        // set the visibility of the button
        button.setVisible(true);
        // set the size of the button
        button.setSize(100, 100);
        frame.add(ta);
        frame.add(tResult);
        frame.add(panel);
        panel.add(button);
        panel.add(tlbl);

        // set the visibility of the button
        buttonAddF.setVisible(true);
        // set the size of the button
        buttonAddF.setSize(100, 100);
        frame.add(friendName);
        frame.add(tResultOfAddingFriend);
        frame.add(panelFriendAdd);
        panelFriendAdd.add(buttonAddF);
        panelFriendAdd.add(friendToAdd);
    }

    private void addMultipleFriends(String[] parsedFNs) {
        Thread addFL = new Thread(new Runnable() {
            @Override
            public void run() {
                for(String friend : parsedFNs) {
                    if (!p.addToFriendsList(friend)) {
                        tResultOfAddingFriend.setText("Failed to add friend: " + friend);
                        return;
                    }
                }
                tResultOfAddingFriend.setText("Added all friends to list!");
            }
        });
        addFL.start();
    }

    public String AddAndRemoveUsers(){
        try {
            String[] LargeFL = new String[200];

            ExecutorService s = Executors.newFixedThreadPool(4);

            // Make a new Thread to add a several friends to an array
            Runnable addFriends = () -> {
                for (int i = 0; i < 200; i++) {
                    p.addToFriendsList("FriendNumber" + i);
                }
            };

            // Make a new Thread to remove a several friends
            Runnable removeFriends = () -> {
                for (int i = 0; i < 200; i *= 2) {
                    p.removeFromFriendsList("FriendNumber" + i);
                }
            };

            // Make a new Thread to check the existence a several friends
            // Note: depending on where the other threads are in the calling process,
            //       this might return true for some friends and false for others since
            //       threads are running in the background with others.
            Runnable checkIfFExists = () -> {
                for (int i = 0; i < 200; i++) {
                    System.out.println(p.DoesFriendExist("FriendNumber" + i));
                }
            };

            // Another way to add friends
            Runnable addFriendsToArr = () -> {
                for (int i = 0; i < 200; i++) {
                    LargeFL[i] = "FriendNum" + i;
                }
            };

            Runnable addFriends2 = () -> {
                p.largeFriendsListToAdd = LargeFL;
                p.run();
            };

            s.submit(addFriends);
            s.submit(removeFriends);
            s.submit(addFriendsToArr);
            s.submit(addFriends2);
            s.submit(checkIfFExists);
        }catch (Exception e){
            return "Error: " + e;
        }
        return "Complete!";
    }
}
